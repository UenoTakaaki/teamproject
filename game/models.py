from django.db import models
from django.utils import timezone

# Create your models here.

class Post(models.Model):
    game_name=models.TextField()
    genre=models.TextField()
    title=models.TextField()
    body=models.TextField()
    like=models.IntegerField(default=0)
    posted_at=models.DateTimeField(default=timezone.now)
    picture = models.ImageField(upload_to='images/')

class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    text=models.TextField()
    posted_at=models.DateTimeField(default=timezone.now)
    