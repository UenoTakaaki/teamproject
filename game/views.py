from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.utils import timezone
from game.models import Post, Comment
from django.db.models import Q 

# Create your views here.

def home(request):
    article = Post.objects.all().order_by('-like')  
    context = {
        'article': article,
        }
    query_t = request.GET.get('search_title')
    query_g = request.GET.get('search_genre')
    if query_t:
        article = Post.objects.all().order_by('-like')
        article = article.filter(
        Q(game_name__icontains=query_t)|
        Q(title__icontains=query_t)
        ).distinct()
        context = {
            'article': article,
            'query_t': query_t,
            }
    elif query_g:
        article = Post.objects.all().order_by('-like')
        article = article.filter(
        Q(genre__icontains=query_g)
        )
        context = {
            'article': article,
            'query_g': query_g,
            }
    return render(request, 'game/home.html', context)

def detail(request,id):
    try:
        article = Post.objects.get(id=id)
    except Post.DoesNotExist:
        raise Http404("Article does not exist")

    if request.method == 'POST':
        comment = Comment(post=article, text=request.POST['text'])
        comment.save()

    context = {
        "article": article,
        "comments": article.comments.order_by('-id')
        }
    return render(request, 'game/detail.html', context,)

def post(request):
    if request.method == 'POST':

        if 'upload_file' in request.FILES:
            post = Post(game_name=request.POST['game'], genre=request.POST['genre'], title=request.POST['title'], body=request.POST['text'], picture=request.FILES['upload_file'])
        else:
            post = Post(game_name=request.POST['game'], genre=request.POST['genre'], title=request.POST['title'], body=request.POST['text'])
        post.save()

        context = {
            "article": post
        }
        return render(request, 'game/detail.html', context)
    else:
        return render(request, 'game/post.html')
        
def like(request, id):
    try:
        article = Post.objects.get(id=id)
        article.like += 1
        article.save()

    except Post.DoesNotExist:
        raise Http404("Article does not exist")
    
    return redirect(detail,id)